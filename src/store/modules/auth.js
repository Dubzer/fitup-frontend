import cookies from 'js-cookie'
import {AUTH_CHECK, AUTH_ERROR, AUTH_LOGIN, AUTH_LOGOUT, AUTH_SIGNUP, AUTH_SUCCESS} from "@/store/actions/auth";
import axios from 'axios'

const state = {
    token: cookies.get('token'),
    checked: undefined,
    error: undefined
}

const getters = {
    isTokened: state => !!state.token,
    isChecked: state => !!state.checked,
};

const actions = {
    [AUTH_CHECK]: ({commit, dispatch}) => {
        return new Promise((resolve, reject) => {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + state.token;
            axios.get('https://api.dubzer.dev/fitup/login/check')
                .then(resp => {
                    commit(AUTH_SUCCESS, resp.data);
                    resolve();
                })
                .catch(ex => {
                    commit(AUTH_ERROR, ex);
                    dispatch(AUTH_LOGOUT);
                    reject(ex);
                });
        })
    },

    [AUTH_LOGIN]: ({ commit, dispatch }, user) => {
        return new Promise((resolve, reject) => {
            axios.get(`https://api.dubzer.dev/fitup/login?username=${user.username}&password=${user.password}`)
                .then(resp => {
                    cookies.set("token", resp.data, {expires: 31}); //standart expiry 31 days
                    commit(AUTH_SUCCESS, resp.data);
                    dispatch(AUTH_CHECK);
                    resolve();
                })
                .catch(ex => {
                    commit(AUTH_ERROR, ex);
                    dispatch(AUTH_LOGOUT);
                    reject(ex);
                });
        });
    },
    [AUTH_SIGNUP]: ({ commit, dispatch }, user) => {
        return new Promise((resole, reject) => {
            axios.post(`https://api.dubzer.dev/fitup/login/register?username=${user.username}&password=${user.password}`)
                .then(resp => {
                    cookies.set("token", resp.data, {expires: 31}); //standart expiry 31 days
                    commit(AUTH_SUCCESS, resp.data);
                    dispatch(AUTH_CHECK);
                    resole();
                })
                .catch(ex => {
                    commit(AUTH_ERROR, ex);
                    dispatch(AUTH_LOGOUT);
                    reject(ex);
                });
        });
    },
    // eslint-disable-next-line no-unused-vars
    [AUTH_LOGOUT]:  ({ commit }) => {
        return new Promise(resolve => {
            console.error("Auth expired")
            cookies.remove('token');
            axios.defaults.headers.common['Authorization'] = null;
            commit(AUTH_LOGOUT);
            resolve();
        });
    },

}

const mutations = {
    [AUTH_SUCCESS]: (state, token) => {
        state.error = null;
        state.token = token;
        state.checked = true;
    },
    [AUTH_ERROR]: (state, error) => {
        state.error = error;
        state.token = null;
        state.checked = false;
    },
    [AUTH_LOGOUT]: (state) => {
        state.error = null;
        state.token = null;
        state.checked = false;
    }
};

export default { state, getters, actions, mutations}
