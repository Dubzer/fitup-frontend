import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'
import {AUTH_CHECK} from "@/store/actions/auth";
import Home from '../views/Home'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    meta: {
      auth: true
    },
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "login" */ '../views/Login')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})


router.beforeEach((to, from, next) => {
  if(to.meta.auth) { //auth required
    if(store.getters.isTokened && store.getters.isChecked) {
      next();
    } //authed
    else if(store.getters.isTokened){
      store.dispatch(AUTH_CHECK).then(() => next()) //token and user got validated
          .catch(() => next('/login')) //token here, user not valid - reauth
    }
    else next('/login') //no token
  }
  else next(); //no auth required
})

export default router
